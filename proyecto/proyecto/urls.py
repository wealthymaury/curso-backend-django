"""proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from app.views import EnlaceListView, EnlaceDetailView
from django.contrib import admin
from django.conf.urls import include, url
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = [
    # URLs para la primer clase

	# url(r'^$', 'app.views.home', name='home'),
	# url(r'^post/(\d+)$', 'app.views.post', name='post'),
	# url(r'^curso/(\d+)/(\d)$', 'app.views.curso', name='curso'),
	# url(r'^consumo/$', 'app.views.consumo', name='consumo'),
    # url(r'^admin/$', include(admin.site.urls)),

    # URLs para la segunda clase
    # url(r'^$', 'app.views.hora_actual', name='hora'),
    url(r'^admin/', include(admin.site.urls)),

    # URLs de Puls3
    url(r'^$', 'app.views.home', name="home"),
    url(r'^plus/(\d+)$', 'app.views.plus', name="plus"),
    url(r'^minus/(\d+)$', 'app.views.minus', name="minus"),
    url(r'^categoria/(\d+)$', 'app.views.categoria', name="categoria"),
    url(r'^add/$', 'app.views.add', name="add"),

    # Class Based Views
    url(r'^about/$', TemplateView.as_view(template_name='index.html'), name="about"),
    url(r'^enlaces/$', EnlaceListView.as_view(), name="enlaces"),
    url(r'^enlaces/(?P<pk>[\d]+)$', EnlaceDetailView.as_view(), name="enlace"),
]
