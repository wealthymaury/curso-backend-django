from django.contrib import admin
from models import Enlace, Categoria, Agregador

from actions import export_as_csv

class EnlaceAdmin(admin.ModelAdmin):
	# votos_string es un campo virtual del modelo original, render_votos_string es una funcion de este mismo modelo
	list_display = ('id', 'titulo', 'enlace', 'categoria', 'votos_string', 'render_votos_imagen', 'es_popular')
	list_filter = ('categoria', 'usuario')
	search_fields = ('categoria__titulo', 'usuario__email')
	list_editable = ('titulo', 'enlace', 'categoria')
	list_display_links = ('id', 'es_popular',)
	actions = [export_as_csv]
	raw_id_fields = ('categoria', 'usuario')

	def render_votos_imagen(self, obj):
		url = obj.votos_imagen()
		tag = '<img src="%s" />' % url
		return tag

	render_votos_imagen.allow_tags = True
	render_votos_imagen.admin_order_field = 'votos'

class EnlaceInline(admin.StackedInline):
    model = Enlace
    extra = 1
    raw_id_fields = ('usuario',)

class CategoriaAdmin(admin.ModelAdmin):
	list_display = ('id', 'titulo')
	actions = [export_as_csv]
	inlines = [EnlaceInline]

class AgregadorAdmin(admin.ModelAdmin):
	list_display = ('id', 'titulo')
	filter_horizontal = ('enlaces',)

# Register your models here.
admin.site.register(Agregador, AgregadorAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Enlace, EnlaceAdmin)