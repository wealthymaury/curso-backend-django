from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from .models import Categoria, Enlace

# Create your tests here.
class testing(TestCase):
	# este metodo se corre antes de cada prueba
	def setUp(self):
		self.categoria = Categoria.objects.create(titulo="Categoria de prueba")
		self.usuario = User.objects.create_user(username='maury', password='1234')

	# Esta prueba conprueba que un enlace tenga la categorizacion adecuada segun el numero de votos
	def test_es_popular(self):
		enlace = Enlace.objects.create(enlace='http://www.google.com', titulo='titulo', votos=0, categoria=self.categoria, usuario=self.usuario)

		# Si un enlace tiene menos de 10 votos no es popular
		self.assertEqual(enlace.votos, 0)
		self.assertEqual(enlace.es_popular(), False)
		self.assertFalse(enlace.es_popular())

		enlace.votos = 20
		enlace.save()

		self.assertEqual(enlace.votos, 20)
		self.assertEqual(enlace.es_popular(), True)
		self.assertTrue(enlace.es_popular())

	def test_view(self):
		res = self.client.get(reverse('home'))
		self.assertEqual(res.status_code, 200)

		res = self.client.get(reverse('about'))
		self.assertEqual(res.status_code, 200)

		res = self.client.get(reverse('enlaces'))
		self.assertEqual(res.status_code, 200)

		# simulando un login
		res = self.client.login(username='maury', password='1234')
		self.assertTrue(res)

		# esta vista requiere un login por decorador
		res = self.client.get(reverse('add'))
		self.assertEqual(res.status_code, 200)

	def test_add(self):
		# me logueo
		res = self.client.login(username='maury', password='1234')
		self.assertTrue(res)

		# verifico que no existan enlaces
		self.assertEqual(Enlace.objects.count(), 0)

		# creo un diccionario con los datos que simulare enviar por POST
		data = {}
		data['titulo'] = 'Test'
		data['enlace'] = 'http://mejorando.la/'
		data['categoria'] = self.categoria.id

		# hago en envio de los datos por POST
		res = self.client.post(reverse('add'), data)

		# verifico que la direccion que espero sea la que se hace
		self.assertEqual(res.status_code, 302)

		# validar que una vez correcto el envio de datos, ya exista un enlace en la DB
		self.assertEqual(Enlace.objects.count(), 1)

		# verificando que los datos guardados sean los correctos
		enlace = Enlace.objects.all()[0]
		self.assertEqual(enlace.titulo, data['titulo'])
		self.assertEqual(enlace.enlace, data['enlace'])
		self.assertEqual(enlace.categoria, self.categoria)


