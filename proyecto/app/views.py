from django.http import HttpResponse, HttpResponseRedirect
from django.template import Context
from django.shortcuts import render_to_response, get_object_or_404, render
from django.views.generic import ListView, DetailView
from django.template.loader import get_template
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from datetime import datetime
from models import *
from forms import *

import urllib2
import json

# Create your views here.

def home(request):
	categorias = Categoria.objects.all()
	enlaces = Enlace.objects.order_by('-votos').all()
	data = { 'enlaces' : enlaces, 'categorias' : categorias }
	template = 'index.html'
	
	return render(request, template, data)

@login_required
def plus(request, enlace_id):
	enlace = Enlace.objects.get(pk = enlace_id)
	enlace.votos = enlace.votos + 1
	enlace.save()

	return HttpResponseRedirect('/')

@login_required
def minus(request, enlace_id):
	enlace = Enlace.objects.get(pk = enlace_id)
	enlace.votos = enlace.votos - 1
	enlace.save()

	return HttpResponseRedirect('/')

def categoria(request, categoria_id):
	categorias = Categoria.objects.all()
	#cat = Categoria.objects.get( pk = categoria_id )
	cat = get_object_or_404(Categoria, pk = categoria_id)
	enlaces = Enlace.objects.filter(categoria = cat).order_by('-votos')
	data = RequestContext(request, { 'enlaces' : enlaces, 'categorias' : categorias })

	return render_to_response('index.html', data)

@login_required
def add(request):
	categorias = Categoria.objects.all()
	if request.method == "POST":
		form = EnlaceForm(request.POST)
		if form.is_valid():
			enlace = form.save(commit = False)
			enlace.usuario = request.user
			enlace.save()
			return HttpResponseRedirect('/')
	else:
		form = EnlaceForm()

	template = 'form.html'
	data = RequestContext(request, locals())
	return render_to_response(template, data)

class EnlaceListView(ListView):
	model = Enlace 
	template_name = 'index.html'
	context_object_name = 'enlaces'

class EnlaceDetailView(DetailView):
	model = Enlace 
	template_name = 'index.html'


# EJEMPLOS de clase

# def home(request):
#     return HttpResponse('Hola mundo')

# def post(request, id_post):
# 	return HttpResponse('hola cabron %s' % (id_post))

# def curso(request, id_post, id_comm):
# 	return HttpResponse('hola cabron %s que pedo con %s' % (id_post, id_comm))

# def consumo(request):
# 	try:
# 		f = urllib2.urlopen('http://congresorest.appspot.com/diputado/3')
# 		g = f.read()
# 		f.close()

# 	except urllib2.HTTPError, e:
# 		print 'Error!!'
# 		print e.code

# 	except urllib2.URLError, e:
# 		print 'Error!!'
# 		print e.code

# 	dic = json.loads(g)

# 	return HttpResponse(dic['entidad'])

# def hora_actual(request):
# 	# Modo largo

# 	ahora = datetime.now()
# 	# template = get_template("hora.html")
# 	# context = Context({ "hora" : ahora, "usuario" : "Mauricio"})
# 	# html = template.render(context)

# 	# return HttpResponse(html);

# 	#Modo corto
# 	return render_to_response("hora.html", { 'hora' : ahora, 'usuario' : 'maury' })
