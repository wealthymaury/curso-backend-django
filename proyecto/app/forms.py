from django import forms
from django.forms import ModelForm
from models import *

class EnlaceForm(forms.ModelForm):
    class Meta:
        model = Enlace
        exclude = ('votos', 'usuario')
